<?php

return [
    'db' => [
        'host'     => 'db01',
        'port'     => '',
        'username' => 'shopware',
        'password' => 'shopware',
        'dbname'   => 'shopware',
    ],
    'front'          => [
        'showException'   => true,
        'throwExceptions' => true,
        'noErrorHandler'  => false,
    ],
    'template'       => [
        'forceCompile' => false,
    ],
    //Model-Cache
    'model'          => [
        'cacheProvider' => 'auto' // supports Apc, Array, Wincache and Xcache
    ],
    'snippet'        => [
        'readFromDb'             => true,
        'writeToDb'              => true,
        'readFromIni'            => true,
        'writeToIni'             => true,
        'showSnippetPlaceholder' => true,
    ],
    'cache'          => [
        'backend'         => 'Black-Hole',
        'backendOptions'  => [],
        'frontendOptions' => [
            'write_control' => false,
        ],
    ],
    'phpsettings'    => [
        'error_reporting' => E_ALL & ~E_USER_DEPRECATED,
        'html_errors'     => true,
        'display_errors'  => 1,
        'error_log'       => 'logs/core_' . $this->Environment() . '-' . date('Y-m-d') . '.log',
    ],
    'csrfProtection' => [
        'frontend' => false,
        'backend'  => false,
    ]
];
